# frozen_string_literal: true

module Rodauth
  module OAuth
    VERSION = "1.0.0"
  end
end
